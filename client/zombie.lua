zombieClass = {}
zombieClass.__index = zombieClass

function zombieClass.new()
	local zombieClass_object = setmetatable({
		bitmap = love.graphics.newImage("res/zombie.png"),
		x = 0,
		y = 0,
		dx = 0,
		dy = 0,
		rotation = 1,
		velocity = 50,
		damage = 0,
		health = 10,
		position = 0,
		dead = false
	}, zombieClass)
	return zombieClass_object
end
--
function zombieClass:getBitmap()
	return self.bitmap
end
function zombieClass:setBitmap(bitmap)
	self.bitmap = bitmap
end
--
function zombieClass:getX()
	return self.x
end
function zombieClass:setX(x)
	self.x = x
end
--
function zombieClass:getDead()
	return self.dead
end
function zombieClass:setDead(x)
	self.dead = x
end
--
function zombieClass:getPosition()
	return self.position
end
function zombieClass:setPosition(x)
	self.position = position
end
--
function zombieClass:getHealth()
	return self.health
end
function zombieClass:hurt(x)
	self.health = self.health - x
end
--
function zombieClass:getY()
	return self.y
end
function zombieClass:setY(y)
	self.y = y
end
--
function zombieClass:getDeltaX()
	return self.dx
end
function zombieClass:setDeltaX(dx)
	self.dx = dx
end
--
function zombieClass:getDeltaY()
	return self.dy
end
function zombieClass:setDeltaY(dy)
	self.dy = dy
end
--
function zombieClass:getRotation()
	return self.rotation
end
function zombieClass:setRotation(rotation)
	self.rotation = rotation
end
--
function zombieClass:getVelocity()
	return self.velocity
end
function zombieClass:setVelocity(velocity)
	self.velocity = velocity
end
--
function zombieClass:getDamage()
	return self.damage
end
function zombieClass:setDamage(damage)
	self.damage = damage
end
--

function zombieClass:isInside(x,y)
	if(x > self.x - self.bitmap:getWidth()/2 and x < self.x + self.bitmap:getWidth() and y > self.y - self.bitmap:getHeight()/2 and y < self.y + self.bitmap:getHeight()) then
		return true
	else
		return false
	end
end

function zombieClass:move(dt)
	if(self.health  > 0) then
   		local dx = math.cos(self.rotation) * (dt * self.velocity)
   		local dy = math.sin(self.rotation) * (dt * self.velocity)
   		self.x = self.x + dx
   		self.y = self.y + dy
   	end
end