tileClass = {}
tileClass.__index = tileClass

function tileClass.new()
	local tileClass_object = setmetatable({
		bitmap = love.graphics.newImage("res/tiles/grass.png"),
		x = 0,
		y = 0,
		dx = 0,
		dy = 0,
		rotation = 0,
		velocity = 0,
		damage = 3,
		active = true,
		id = "",
		index = 0
	}, tileClass)
	return tileClass_object
end
--
function tileClass:getBitmap()
	return self.bitmap
end
function tileClass:setBitmap(bitmap)
	self.bitmap = bitmap
end
--
function tileClass:getIndex()
	return self.index
end
function tileClass:setIndex(x)
	self.index = x
end
--
function tileClass:getId()
	return self.id
end
function tileClass:setId(x)
	self.id = x
end
--
function tileClass:getX()
	return self.x
end
function tileClass:setX(x)
	self.x = x
end
--
function tileClass:getY()
	return self.y
end
function tileClass:setY(y)
	self.y = y
end
--
function tileClass:getDeltaX()
	return self.dx
end
function tileClass:setDeltaX(dx)
	self.dx = dx
end
--
function tileClass:getDeltaY()
	return self.dy
end
function tileClass:setDeltaY(dy)
	self.dy = dy
end
--
function tileClass:getRotation()
	return self.rotation
end
function tileClass:setRotation(rotation)
	self.rotation = rotation
end
--
function tileClass:getVelocity()
	return self.velocity
end
function tileClass:setVelocity(velocity)
	self.velocity = velocity
end
--
function tileClass:getDamage()
	return self.damage
end
function tileClass:setDamage(damage)
	self.damage = damage
end
--
function tileClass:move(dt)
	local dx = math.cos(self.rotation) * (dt * self.velocity)
   	local dy = math.sin(self.rotation) * (dt * self.velocity)
   	self.x = self.x + dx
   	self.y = self.y + dy
end

function tileClass:isInside(x,y)
	if(x > self.x - self.bitmap:getWidth()/2 and x < self.x + self.bitmap:getWidth() and y > self.y - self.bitmap:getHeight()/2 and y < self.y + self.bitmap:getHeight()) then
		return true
	else
		return false
	end
end