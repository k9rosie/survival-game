mouseClass = {}
mouseClass.__index = mouseClass

function mouseClass.new()
	local mouseClass_object = setmetatable({
		x = love.mouse.getX(),
		y = love.mouse.getY()
	}, mouseClass)
	return mouseClass_object
end
--
function mouseClass:getX()
	return self.x
end
function mouseClass:setX(x)
	self.x = x
end
--
function mouseClass:getY()
	return self.y
end
function mouseClass:setY(y)
	self.y = y
end