mapClass = {}
mapClass.__index = mapClass

function mapClass.new()
	local mapClass_object = setmetatable({
		bitmap = love.graphics.newImage("res/doge.jpg"),
		x = 0,
		y = 0
		}, mapClass)
	return mapClass_object
end
--
function mapClass:getBitmap()
	return self.bitmap
end
function mapClass:setBitmap(bitmap)
	self.bitmap = bitmap
end
--
function mapClass:getX()
	return self.x
end
function mapClass:setX(x)
	self.x = x
end
--
function mapClass:getY()
	return self.y
end
function mapClass:setY(y)
	self.y = y
end
