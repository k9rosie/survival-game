playerClass = {}
playerClass.__index = playerClass

function playerClass.new()
	local playerClass_object = setmetatable({
		x = 0,
		y = 0,
		health = 10,
		maxhealth = 10,
		bitmap = love.graphics.newImage("res/player.png"),
		rotation = 0,
		velocity = 100,
		vision = 300,
		weapon = {accuracy = 20, projectiles = 4, ammo = 15}

	}, playerClass)
	return playerClass_object
end
--
function playerClass:getVision()
	return self.vision
end
function playerClass:setVision(x)
	self.vision = x
end
--
function playerClass:getWeapon()
	return self.weapon
end
function playerClass:setWeapon(x)
	self.weapon = x
end
--
function playerClass:getX()
	return self.x
end
function playerClass:setX(x)
	self.x = x
end
--
function playerClass:getY()
	return self.y
end
function playerClass:setY(y)
	self.y = y
end
--
function playerClass:getBitmap()
	return self.bitmap
end
function playerClass:setBitmap(bitmap)
	self.bitmap = bitmap
end
--
function playerClass:getRotation()
	return self.rotation
end
function playerClass:setRotation(rotation)
	self.rotation = rotation
end
--
function playerClass:getVelocity()
	return self.velocity
end
function playerClass:setVelocity(velocity)
	self.velocity = velocity
end
--
function playerClass:move(dx, dy)
	self.x = self.x + (dx or 0)
	self.y = self.y + (dy or 0)

	

end

function playerClass:getDistance(x , y)
	return math.sqrt(math.pow(x - self.x, 2) + math.pow(y - self.y, 2))
end

function playerClass:update()
	if(self.rotation * 100 >= -190 and self.rotation * 100 < -110) then
		self.bitmap = love.graphics.newImage("res/player/n.png")
	end
	if(self.rotation * 100 >= -270 and self.rotation * 100 < -190) then
		self.bitmap = love.graphics.newImage("res/player/nw.png")
	end
	if(self.rotation * 100 >= -110 and self.rotation * 100 < -45) then
		self.bitmap = love.graphics.newImage("res/player/ne.png")
	end
	if((self.rotation * 100 >= -313 and self.rotation * 100 < -270) or (self.rotation * 100 >= 280 and self.rotation * 100 < 313)) then
		self.bitmap = love.graphics.newImage("res/player/w.png")
	end
	if(self.rotation * 100 >= 210 and self.rotation * 100 < 280) then
		self.bitmap = love.graphics.newImage("res/player/sw.png")
	end
	if(self.rotation * 100 >= 110 and self.rotation * 100 < 210) then
		self.bitmap = love.graphics.newImage("res/player/s.png")
	end
	if(self.rotation * 100 >= 45 and self.rotation * 100 < 110) then
		self.bitmap = love.graphics.newImage("res/player/se.png")
	end
	if((self.rotation * 100 >= 0 and self.rotation * 100 < 45) or (self.rotation * 100 >= -45 and self.rotation * 100 < 0)) then
		self.bitmap = love.graphics.newImage("res/player/e.png")
	end
end