cameraClass = {}
cameraClass.__index = cameraClass

function cameraClass.new()
	local cameraClass_object = setmetatable({
		x = 0,
		y = 0,
		scaleX = 1, --the default amount our objects can scale in size
		scaleY = 1,
		min_scale = 0.5, --the minimum amount our objects can scale in size
		max_scale = 5, --the maximum amount our objects can scale in size
		rotation = 0
	}, cameraClass)
	return cameraClass_object
end 
--
function cameraClass:getX()
	return self.x
end
function cameraClass:setX(x)
	self.x = x
end
--
function cameraClass:getY()
	return self.y
end
function cameraClass:setY(y)
	self.y = y
end
--
function cameraClass:getScaleX()
	return self.scaleX
end
function cameraClass:setScaleX(scaleX)
	self.scaleX = scaleX
end
--
function cameraClass:getScaleY()
	return self.scaleY
end
function cameraClass:setScaleX(scaleY)
	self.scaleX = scaleY
end
--
function cameraClass:getMinScale()
	return self.min_scale
end
function cameraClass:setMinScale(min_scale)
	self.min_scale = min_scale
end
--
function cameraClass:getMaxScale()
	return self.max_scale
end
function cameraClass:setMaxScale(max_scale)
	self.max_scale = max_scale
end
--
function cameraClass:getRotation()
	return self.rotation
end
function cameraClass:setRotation(rotation)
	self.rotation = rotation
end
--
function cameraClass:set()
	love.graphics.push()
	love.graphics.rotate(self.rotation)
	love.graphics.translate(love.window.getWidth()/2, love.window.getHeight()/2)
	love.graphics.scale(1 / self.scaleX, 1 / self.scaleY)
	love.graphics.translate(-self.x, -self.y)
end

function cameraClass:unset()
	love.graphics.pop()
end

function cameraClass:rotate(rotation)
	self.rotation = self.rotation + rotation
end

function cameraClass:scale(scaleX, scaleY)
	scaleX = scaleX or 1
	self.scaleX = self.scaleX + scaleX
	self.scaleY = self.scaleY + (scaleY or scaleX)
end

function cameraClass:move(dx, dy)
	self.x = self.x + (dx or 0)
	self.y = self.y + (dy or 0)
end

function cameraClass:setPosition(x, y)
	self.x = x or self.x
	self.y = y or self.y
end
