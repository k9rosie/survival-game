windowClass = {}
windowClass.__index = windowClass

function windowClass.new()
	local windowClass_object = setmetatable({
		width = 800,
		height = 600,
	    flags = {
	    	fullscreen = false,
	    	vsync = false,
	    	borderless = false,
	    	fsaa = 0,
	    	resizable = false
	    }
	}, windowClass)
	return windowClass_object
end
--
function windowClass:getWidth()
	return self.width
end
function windowClass:setWidth(width)
	self.width = width
end
--
function windowClass:getHeight()
	return self.height
end
function windowClass:setHeight(height)
	self.height = height
end
--
function windowClass:getFlags()
	return self.flags
end
function windowClass:setFlags(fullscreen, vsync, borderless, fsaa, resizable)
	self.flags = {
		fullscreen = fullscreen,
		vsync = vsync,
		borderless = borderless,
		fsaa = fsaa,
		resizable = resizable
	}
end