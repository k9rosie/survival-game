bulletClass = {}
bulletClass.__index = bulletClass

function bulletClass.new()
	local bulletClass_object = setmetatable({
		bitmap = love.graphics.newImage("res/bullet.png"),
		x = 0,
		y = 0,
		dx = 0,
		dy = 0,
		rotation = 0,
		velocity = 1000,
		damage = 3,
		active = true
	}, bulletClass)
	return bulletClass_object
end
--
function bulletClass:getBitmap()
	return self.bitmap
end
function bulletClass:setBitmap(bitmap)
	self.bitmap = bitmap
end
--
function bulletClass:getActive()
	return self.active
end
function bulletClass:setActive(tf)
	self.active = tf
end
--
function bulletClass:getX()
	return self.x
end
function bulletClass:setX(x)
	self.x = x
end
--
function bulletClass:getY()
	return self.y
end
function bulletClass:setY(y)
	self.y = y
end
--
function bulletClass:getDeltaX()
	return self.dx
end
function bulletClass:setDeltaX(dx)
	self.dx = dx
end
--
function bulletClass:getDeltaY()
	return self.dy
end
function bulletClass:setDeltaY(dy)
	self.dy = dy
end
--
function bulletClass:getRotation()
	return self.rotation
end
function bulletClass:setRotation(rotation)
	self.rotation = rotation
end
--
function bulletClass:getVelocity()
	return self.velocity
end
function bulletClass:setVelocity(velocity)
	self.velocity = velocity
end
--
function bulletClass:getDamage()
	return self.damage
end
function bulletClass:setDamage(damage)
	self.damage = damage
end
--
function bulletClass:move(dt)
	local dx = math.cos(self.rotation) * (dt * self.velocity)
   	local dy = math.sin(self.rotation) * (dt * self.velocity)
   	self.x = self.x + dx
   	self.y = self.y + dy
end