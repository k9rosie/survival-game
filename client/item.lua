itemClass = {}
itemClass.__index = itemClass

function itemClass.new()
	local itemClass_object = setmetatable({
		bitmap = love.graphics.newImage("res/item.png"),
		x = 0,
		y = 0,
		dx = 0,
		dy = 0,
		rotation = 0,
		velocity = 3000,
		damage = 3,
		active = true,
		id = "",
		index = 0
	}, itemClass)
	return itemClass_object
end
--
function itemClass:getBitmap()
	return self.bitmap
end
function itemClass:setBitmap(bitmap)
	self.bitmap = bitmap
end
--
function itemClass:getActive()
	return self.active
end
function itemClass:setActive(tf)
	self.active = tf
end
--
function itemClass:getIndex()
	return self.index
end
function itemClass:setIndex(x)
	self.index = x
end
--
function itemClass:getId()
	return self.id
end
function itemClass:setId(x)
	self.id = x
end
--
function itemClass:getX()
	return self.x
end
function itemClass:setX(x)
	self.x = x
end
--
function itemClass:getY()
	return self.y
end
function itemClass:setY(y)
	self.y = y
end
--
function itemClass:getDeltaX()
	return self.dx
end
function itemClass:setDeltaX(dx)
	self.dx = dx
end
--
function itemClass:getDeltaY()
	return self.dy
end
function itemClass:setDeltaY(dy)
	self.dy = dy
end
--
function itemClass:getRotation()
	return self.rotation
end
function itemClass:setRotation(rotation)
	self.rotation = rotation
end
--
function itemClass:getVelocity()
	return self.velocity
end
function itemClass:setVelocity(velocity)
	self.velocity = velocity
end
--
function itemClass:getDamage()
	return self.damage
end
function itemClass:setDamage(damage)
	self.damage = damage
end
--
function itemClass:move(dt)
	local dx = math.cos(self.rotation) * (dt * self.velocity)
   	local dy = math.sin(self.rotation) * (dt * self.velocity)
   	self.x = self.x + dx
   	self.y = self.y + dy
end

function itemClass:isInside(x,y)
	if(x > self.x - self.bitmap:getWidth()/2 and x < self.x + self.bitmap:getWidth() and y > self.y - self.bitmap:getHeight()/2 and y < self.y + self.bitmap:getHeight()) then
		return true
	else
		return false
	end
end