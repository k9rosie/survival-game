require("player") --player class
require("bullet") --bullet class
require("map") --map class
require("window") --window object
require("mouse") --mouse object
require("camera")
require("zombie")
require("item")
require("tile")

function love.load()

  MAP_WIDTH = 50
  MAP_HEIGHT = 50

  bullets = {} --list of bullets in game
  enemies = {} --list of enemies in game
  items = {} --list of items in game
  map = {}
  for i = 1, MAP_WIDTH do
    map[i] = {}

    for j = 1, MAP_HEIGHT do
        map[i][j] = tileClass.new()
        map[i][j]:setX(i * map[i][j]:getBitmap():getWidth())
        map[i][j]:setY(j * map[i][j]:getBitmap():getHeight())
        if(math.random(10) == 1) then
          map[i][j]:setBitmap(love.graphics.newImage("res/tiles/flowers.png"))
        end
    end
  end

  player = playerClass.new()
  player:setX(map[1][1]:getBitmap():getWidth() * MAP_WIDTH / 2)
  player:setY(map[1][1]:getBitmap():getHeight() * MAP_HEIGHT / 2)
  window = windowClass.new()
  mouse = mouseClass.new()
  camera = cameraClass.new()
  camera:setX(map[1][1]:getBitmap():getWidth() * MAP_WIDTH / 2)
  camera:setY(map[1][1]:getBitmap():getHeight() * MAP_HEIGHT / 2)

  love.window.setMode(window:getWidth(), window:getHeight(), window:getFlags())

  canFire = true
end

function love.update(dt)
  mouse:setX(love.mouse.getX())
  mouse:setY(love.mouse.getY())
  player:setRotation(math.getAngle(mouse:getX(), window:getWidth()/2, mouse:getY(), window:getHeight()/2))
  player:update()


	if love.keyboard.isDown('w') then
    camera:move(0, -player:getVelocity()*dt)
    player:move(0, -player:getVelocity()*dt)
  end
  if love.keyboard.isDown('s') then
    camera:move(0, player:getVelocity()*dt)
    player:move(0, player:getVelocity()*dt)
  end
  if love.keyboard.isDown('a') then
    camera:move(-player:getVelocity()*dt, 0)
    player:move(-player:getVelocity()*dt, 0)
  end
  if love.keyboard.isDown('d') then
    camera:move(player:getVelocity()*dt, 0)
    player:move(player:getVelocity()*dt, 0)
  end
  if love.mouse.isDown("l") then
    if canFire and player:getWeapon().ammo >  0 then
      player:getWeapon().ammo = player:getWeapon().ammo - 1
      for i = 1, player:getWeapon().projectiles do
        local bullet = bulletClass.new()
        bullet:setX(player:getX())
        bullet:setY(player:getY())
        local angle = math.getAngle(mouse:getX(), window:getWidth()/2, mouse:getY(), window:getHeight()/2)
        angle = angle + math.random(player:getWeapon().accuracy) / 100 - (player:getWeapon().accuracy / 100)/2
        bullet:setRotation(angle)

        bullet:setDeltaX(bullet:getVelocity()*math.cos(angle))
        bullet:setDeltaY(bullet:getVelocity()*math.sin(angle))
        table.insert(bullets, bullet)
        canFire = false
      end
    end
  end
  if love.mouse.isDown("l") == false then
    canFire = true
  end

  if math.random(400) == 2 then
    local zombie = zombieClass.new()
    zombie:setX(player:getX()-500+ math.random(1000))
    zombie:setY(player:getY()-500+ math.random(1000))
    zombie:setVelocity(math.random(130) + 20)
    local angle = math.getAngle(player:getX(), zombie:getX(), player:getY(), zombie:getY())
    zombie:setRotation(angle)

    zombie:setDeltaX(zombie:getVelocity()*math.cos(angle))
    zombie:setDeltaY(zombie:getVelocity()*math.sin(angle))

    table.insert(enemies, zombie)
    local count = 0
    for _ in pairs(enemies) do count = count + 1 end
    zombie:setPosition(count)
  end

  for i, bullet in ipairs(bullets) do
    bullet:move(dt)
  end


  for i, item in ipairs(items) do
    item:setIndex(i)

    if item:isInside(player:getX(), player:getY()) and item:getId() == "ammo" then
      player:getWeapon().ammo = player:getWeapon().ammo + math.random(5)
      table.remove(items, item:getIndex())
    end
  end

  for i, zombie in ipairs(enemies) do
    zombie:move(dt)
    angle = math.getAngle(player:getX(), zombie:getX(), player:getY(), zombie:getY())
    if(zombie:getHealth() <=0) then angle = 0 end
    zombie:setRotation(angle)

    for i, bullet in ipairs(bullets) do
     if(zombie:getHealth() > 0 and zombie:isInside(bullet.x + bullet:getBitmap():getWidth()/2, bullet.y+ bullet:getBitmap():getHeight()/2) and bullet:getActive()) then
      zombie:hurt(bullet:getDamage())

      bullet:setActive(false)
     end
    end

    if(zombie:getHealth() <= 0 and zombie:getDead() == false) then
      --table.remove(enemies, 2)
      zombie:setBitmap(love.graphics.newImage("res/deadZombie.png"))
      zombie:setDead(true)

      if math.random(5) == 2 then
        local ammoBox = itemClass.new()

        ammoBox:setX(zombie:getX())
        ammoBox:setY(zombie:getY())
        ammoBox:setRotation(0)
        ammoBox:setId("ammo")
        ammoBox:setBitmap(love.graphics.newImage("res/ammo.png"))
        table.insert(items, ammoBox)
      end

    end
  end
end

function love.mousepressed(x, y, button)
  if button == "wd" then
    if camera:getScaleX() < camera:getMaxScale() then
      camera:scale(0.1)
    end
  elseif button == "wu" then
    if camera:getScaleX() > camera:getMinScale() then
      camera:scale(-0.1)

    end
  end
end

function math.getAngle(x1,x2,y1,y2)
    deltax = x1-x2
    deltay = y1-y2
    return math.atan2(deltay, deltax)
end

function love.draw()
  camera:set()

  for n = 1, MAP_WIDTH do
    for i, tile in ipairs(map[n]) do --draw items
      if player:getDistance(tile:getX() + tile:getBitmap():getWidth()/2, tile:getY() +  tile:getBitmap():getHeight()/2) <= player:getVision() then
        love.graphics.draw(tile:getBitmap(), tile:getX(), tile:getY(), tile:getRotation(), 1, 1, tile:getBitmap():getWidth()/2, tile:getBitmap():getHeight()/2)
      end
    end
  end

  love.graphics.draw(player:getBitmap(), player:getX(), player:getY(), 0, 1, 1, player:getBitmap():getWidth()/2, player:getBitmap():getHeight()/2)
  --^draw player

  for i, bullet in ipairs(bullets) do --draw bullets
    if(bullet:getActive()) then
      love.graphics.draw(bullet:getBitmap(), bullet:getX(), bullet:getY(), bullet:getRotation(), 1, 1, bullet:getBitmap():getWidth()/2, bullet:getBitmap():getHeight()/2)
    end
  end

  for i, zombie in ipairs(enemies) do --draw zombies
    if player:getDistance(zombie:getX() + zombie:getBitmap():getWidth()/2, zombie:getY() +  zombie:getBitmap():getHeight()/2) <= player:getVision() then
      love.graphics.draw(zombie:getBitmap(), zombie:getX(), zombie:getY(), zombie:getRotation(), 1, 1, zombie:getBitmap():getWidth()/2, zombie:getBitmap():getHeight()/2)
    end
  end
  for i, item in ipairs(items) do --draw items
    if player:getDistance(item:getX() + item:getBitmap():getWidth()/2, item:getY() +  item:getBitmap():getHeight()/2) <= player:getVision() then
      love.graphics.draw(item:getBitmap(), item:getX(), item:getY(), item:getRotation(), 1, 1, item:getBitmap():getWidth()/2, item:getBitmap():getHeight()/2)
    end
  end
  

  camera:unset()

  --debug graphics
  love.graphics.print("Debug info:", 1, 0)
  love.graphics.print('Mouse Pos:('..mouse:getX()..', '..mouse:getY()..')', 1, 20)
  love.graphics.print("Current FPS: "..love.timer.getFPS(), 1, 40)
  love.graphics.print("Ammo: "..tostring(player:getWeapon().ammo), 1, 60)
  love.graphics.print("Current scale: ("..camera:getScaleX()..", "..camera:getScaleY()..")", 1, 80)
end